# py_command_wrapper

This repository contains a simple wrapper around the Python
[subprocess](https://docs.python.org/3/library/subprocess.html) module, more
specifically, around the
[run](https://docs.python.org/3/library/subprocess.html#subprocess.run)
function.

## Usage

Should be used as a python module. Can be installed in a Pipfile with
```toml
py_command_wrapper = { git = 'https://gitlab.inria.fr/aljanon-experiment-framework/py_command_wrapper.git', editable = true }
```
It provides five classes:

- Command: Abstract Base Class, the constructor needs a pathlib.Path for the
  command working directory.
- RunArgs: Accepts a list of strings representing the command and its arguments.
  The whole command line, stdout, stderr and the return code are all logged
  at the trace level using the loguru logger.
- RunArgsSaveStdout: Same as RunArgs, except it saves the command's standard
  output at the given path.
- Copyfile: Copies the **contents** of a file from the input path to the output
  path. Both path are fully resolved, following all potential links. No checks
  are done regarding the permissions or the existence of the output file.
  The file's contents are **not** logged.
- CommandList: List of commands. Can be added/removed with `append`/`remove`.

## Dependencies

This module requires [loguru](https://github.com/Delgan/loguru/) for logging.
This module does not configure the logger itself, please see
[py_loguru_wrapper](https://gitlab.inria.fr/aljanon-experiment-framework/py_command_wrapper.git)
instead.
