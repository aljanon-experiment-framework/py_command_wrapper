#!/usr/bin/env python

from setuptools import setup

setup(
    name='py_command_wrapper',
    version='0.1',
    description='Command wrapper for subprocess',
    url='https://gitlab.inria.fr/aljanon/py_command_wrapper',
    author='Alexis Janon',
    author_email='alexis.janon@inria.fr',
    packages=['py_command_wrapper'],
    install_requires=['loguru'],
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)'
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
    ]
)
