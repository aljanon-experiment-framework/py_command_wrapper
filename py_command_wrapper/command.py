#!/usr/bin/python3

# py_command_wrapper
# Copyright (C) 2019  JANON Alexis

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import subprocess
from abc import ABC, abstractmethod
from pathlib import Path
from typing import List, Optional

from loguru import logger


class Command(ABC):
    basedir: Path

    def __init__(self, basedir: Path) -> None:
        self.basedir = basedir.resolve()
        basedir.mkdir(parents=True, exist_ok=True)

    @abstractmethod
    def run(self) -> None:
        pass


class RunArgs(Command):
    args: List[str]

    def __init__(self, basedir: Path, args: List[str]) -> None:
        super().__init__(basedir)
        self.args = args

    def run(self):
        logger.bind(args=self.args).debug(f"Executing: '{' '.join(self.args)}'")
        completed = subprocess.run(
            self.args, cwd=self.basedir, capture_output=True, text=True
        )
        logger.bind(
            stdout=completed.stdout.splitlines(),
            stderr=completed.stderr.splitlines(),
            returncode=completed.returncode,
            args=self.args,
        ).trace(f"'{' '.join(self.args)}' execution returned {completed.returncode}")
        if completed.returncode != 0:
            logger.error(
                f"'{' '.join(self.args)}' execution returned error code: {completed.returncode}"
            )
        else:
            logger.success(f"'{' '.join(self.args)}' completed successfully.")
        return completed


class RunArgsSaveStdout(RunArgs):
    output: Path

    def __init__(self, basedir: Path, args: List[str], output: Path) -> None:
        super().__init__(basedir, args)
        self.output = output

    def run(self):
        completed = super().run()
        logger.bind(args=self.args, output=self.output).debug(
            f"Saving stdout of {' '.join(self.args)} to {self.output}."
        )
        self.output.write_text(completed.stdout)


class Copyfile(Command):
    input: Path
    output: Path

    def __init__(self, basedir: Path, input: Path, output: Path = None) -> None:
        super().__init__(basedir)
        self.input = input.resolve()
        self.output = output
        if output == None:
            self.output = basedir / input.name

    # TODO: should probably 'copy' the file with correct permissions
    def run(self):
        if not self.input.exists():
            logger.bind(input=self.input).error(f"Cannot find '{self.input}'")
            return
        if not self.input.is_file():
            logger.bind(input=self.input).error(f"'{self.input}' is not a file.")
        contents = self.input.read_bytes()
        self.output.write_bytes(self.input.read_bytes())
        logger.bind(input=self.input, output=self.output).debug(
            f"Copied contents of '{self.input}' to '{self.output}'."
        )


class CommandList(Command):
    cmd_list: List[Command]

    def __init__(self, basedir: Path) -> None:
        super().__init__(basedir)
        self.cmd_list = []

    def append(self, cmd: Command) -> None:
        self.cmd_list.append(cmd)

    def remove(self, cmd: Command) -> None:
        self.cmd_list.remove(cmd)

    def pop(self, i: Optional[int] = None) -> Command:
        if i is None:
            return self.pop()
        return self.pop(i)

    def run(self) -> None:
        cls_name: str = self.__class__.__name__
        logger.bind(cmd=cls_name).info(f"Getting information from {cls_name}.")
        for cmd in self.cmd_list:
            cmd.run()
